class Board:

    def __init__(self, N=8, othello=True):
        self.othello = othello
        self.N = N
        self.grid = np.array([
            [Space(i, j) for j in range(N)]
            for i in range(N)
        ])
        for space in np.ravel(self.grid):
            space.find_neighbors(self.grid)
        if othello:
            self('B', 3, 4, force=True)
            self('W', 3, 3, force=True)
            self('B', 4, 3, force=True)
            self('W', 4, 4, force=True)

    def __getitem__(self, *x):
        return self.grid.__getitem__(*x)

    def playergrid(self):
        grid, N = self.grid, self.N
        return np.array([
            [grid[i,j].player for j in range(N)]
            for i in range(N) ])

    def __str__(self):
        grid, N = self.grid, self.N
        lines = [''.join('({:1})'.format(grid[i,j].player) for j in range(N))
                  for i in range(N)]
        lines += ['  ' + ''.join(' {} '.format(i+1) for i in range(N))]
        for (i,line) in enumerate(lines[:-1]):
            lines[i] = '{} '.format(chr(ord('a') + i)) + line
        return '\n'.join(lines)

    def __call__(self, player, i, j, force=False):
        self[i,j].set(player, force=force)
